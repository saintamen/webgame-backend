package com.sda.webgame.repositories;

import com.sda.webgame.model.ColonyLot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface ColonyLotRepository extends CrudRepository<ColonyLot, Long>{

    Optional<ColonyLot> getById(long id);

}

