package com.sda.webgame.model;

public enum BuildingType {
    NONE,           // brak budynku
    TOWN_HALL,      // ratusz
    STONEMASON,     // kamieniarz
    SAWMILL,        // tartak
    HOUSEHOLD,      // budynek mieszkalny
    ACADEMY,        // uczelnia
    FARM;           // farma
}