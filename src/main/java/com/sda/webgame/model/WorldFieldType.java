package com.sda.webgame.model;

public enum WorldFieldType {
    GRASS /*Dostępne do osiedlenia*/,
    MOUNTAIN,
    SWAMP,
    FOREST,
    SEA;
}
